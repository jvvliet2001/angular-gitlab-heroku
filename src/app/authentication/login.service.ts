import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { User } from '../models/user'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public token

  constructor(private http: HttpClient) {}

  validateLogin(user: User, cb) {
    this.http
      .post(
        'http://localhost:3000/api/login',
        {
          emailAddress: user.emailAddress,
          password: user.password
        },
        httpOptions
      )
      .subscribe(res => {
        const json = JSON.stringify(res)
        const obj = JSON.parse(json)
        localStorage.setItem('token', obj.Token)
        cb(obj.Token)
      })
  }

  validateRegister(user: User) {
    return this.http.post(
      'http://localhost:3000/api/register',
      {
        firstName: user.firstName,
        lastName: user.lastName,
        emailAddress: user.emailAddress,
        mobileNumber: user.mobileNumber,
        password: user.password
      },
      httpOptions
    )
  }

  userLogOut() {
    localStorage.removeItem('token')
  }
}
