import { Component } from '@angular/core'
import { LoginService } from '../login.service'
import { NavbarService } from '../../core/navbar/navbar.service'
import { Router } from '@angular/router'
import { User } from '../../models/user'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [LoginService]
})
export class LoginComponent {
  public user: User

  constructor(
    private loginService: LoginService,
    private router: Router,
    private navbarService: NavbarService
  ) {
    this.user = new User()
  }

  ngOnInit() {
    this.navbarService.hide()
  }

  validateLogin() {
    if (this.user.emailAddress && this.user.password) {
      this.loginService.validateLogin(this.user, user => {
        if (user !== undefined) {
          console.log(user)
          this.router.navigate(['/dashboard'])
          this.navbarService.show()
        } else {
          alert('Emailaddress and password are not correct.')
        }
      })
    } else {
      alert('Enter emailaddress and password.')
    }
  }
}
