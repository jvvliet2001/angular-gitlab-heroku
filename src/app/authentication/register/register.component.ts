import { Component } from '@angular/core'
import { LoginService } from '../login.service'
import { User } from '../../models/user'
import { Router } from '@angular/router'
import { NavbarService } from '../../core/navbar/navbar.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [LoginService]
})
export class RegisterComponent {
  public user: User

  constructor(
    private loginService: LoginService,
    private router: Router,
    private navbarService: NavbarService
  ) {
    this.user = new User()
  }

  ngOnInit() {
    this.navbarService.hide()
  }

  validateRegister() {
    if (
      this.user.firstName &&
      this.user.lastName &&
      this.user.emailAddress &&
      this.user.mobileNumber &&
      this.user.password
    ) {
      this.loginService.validateRegister(this.user).subscribe(result => {
        console.log(result)
        alert('Account is successfully created.')
        this.loginService.validateLogin(this.user, user => {
          if (user !== undefined) {
            console.log(user)
            this.router.navigate(['/dashboard'])
            this.navbarService.show()
          } else {
            alert('Something went wrong, please try again.')
          }
        })
      })
    } else {
      alert('Enter valid information.')
    }
  }
}
