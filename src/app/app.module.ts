import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LoginComponent } from './authentication/login/login.component'
import { RegisterComponent } from './authentication/register/register.component'
import { ArtistComponent } from './core/dashboard/artist/artist.component'
import { ConcertComponent } from './core/dashboard/concert/concert.component'
import { SongComponent } from './core/dashboard/song/song.component'
import { ReactiveFormsModule } from '@angular/forms'
import { FormsModule } from '@angular/forms'
import { DashboardRoutingModule } from './core/dashboard/dashboard-routing.module'
import { ArtistCreateComponent } from './core/dashboard/artist/artist-create/artist-create.component'
import { ArtistReadAllComponent } from './core/dashboard/artist/artist-read-all/artist-read-all.component'
import { ArtistUpdateComponent } from './core/dashboard/artist/artist-update/artist-update.component'
import { ArtistDeleteComponent } from './core/dashboard/artist/artist-delete/artist-delete.component'
import { ArtistService } from './core/dashboard/artist/artist.service'
import { ConcertCreateComponent } from './core/dashboard/concert/concert-create/concert-create.component'
import { ConcertReadAllComponent } from './core/dashboard/concert/concert-read-all/concert-read-all.component'
import { ConcertUpdateComponent } from './core/dashboard/concert/concert-update/concert-update.component'
import { ConcertDeleteComponent } from './core/dashboard/concert/concert-delete/concert-delete.component'
import { ConcertService } from './core/dashboard/concert/concert.service'
import { SongCreateComponent } from './core/dashboard/song/song-create/song-create.component'
import { SongReadAllComponent } from './core/dashboard/song/song-read-all/song-read-all.component'
import { SongUpdateComponent } from './core/dashboard/song/song-update/song-update.component'
import { SongDeleteComponent } from './core/dashboard/song/song-delete/song-delete.component'
import { SongService } from './core/dashboard/song/song.service'
import { NavbarService } from './core/navbar/navbar.service'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    ArtistComponent,
    ConcertComponent,
    SongComponent,
    ArtistCreateComponent,
    ArtistReadAllComponent,
    ArtistUpdateComponent,
    ArtistDeleteComponent,
    ConcertCreateComponent,
    ConcertReadAllComponent,
    ConcertUpdateComponent,
    ConcertDeleteComponent,
    SongCreateComponent,
    SongReadAllComponent,
    SongUpdateComponent,
    SongDeleteComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    DashboardRoutingModule
  ],
  providers: [ArtistService, ConcertService, SongService, NavbarService],
  bootstrap: [AppComponent]
})
export class AppModule {}
