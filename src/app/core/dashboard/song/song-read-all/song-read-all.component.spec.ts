import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { SongReadAllComponent } from './song-read-all.component'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('SongReadAllComponent', () => {
  let component: SongReadAllComponent
  let fixture: ComponentFixture<SongReadAllComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SongReadAllComponent],
      imports: [RouterTestingModule, HttpClientTestingModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(SongReadAllComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })
})
