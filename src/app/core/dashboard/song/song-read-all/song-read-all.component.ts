import { Component, OnInit } from '@angular/core'
import { SongService } from '../song.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-song-read-all',
  templateUrl: './song-read-all.component.html',
  styleUrls: ['./song-read-all.component.scss'],
  providers: [SongService]
})
export class SongReadAllComponent implements OnInit {
  public list: object

  constructor(private songService: SongService, private router: Router) {}

  ngOnInit() {
    this.getAllSongs()
  }

  getAllSongs(): void {
    this.songService.getSongs().subscribe(result => (this.list = result))
  }
}
