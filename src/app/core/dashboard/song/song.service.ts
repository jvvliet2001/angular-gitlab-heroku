import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Song } from '../../../models/song'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  })
}

@Injectable({
  providedIn: 'root'
})
export class SongService {
  constructor(private http: HttpClient) {}

  getSongs() {
    return this.http.get('http://localhost:3000/api/songs')
  }

  addSong(song: Song) {
    console.log(localStorage.getItem('token'))
    if (localStorage.getItem('token')) {
      return this.http.post(
        'http://localhost:3000/api/song',
        {
          title: song.title,
          artist: song.artist
        },
        httpOptions
      )
    }
  }
}
