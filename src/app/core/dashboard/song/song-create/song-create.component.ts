import { Component, OnInit } from '@angular/core'
import { Song } from '../../../../models/song'
import { SongService } from '../song.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-song-create',
  templateUrl: './song-create.component.html',
  styleUrls: ['./song-create.component.scss']
})
export class SongCreateComponent implements OnInit {
  public song: Song

  constructor(private songService: SongService, private router: Router) {
    this.song = new Song()
  }

  ngOnInit() {}

  addSong() {
    if (this.song.title && this.song.artist) {
      this.songService.addSong(this.song).subscribe(res => {
        this.router.navigate(['/dashboard/songs/song-readAll'])
        console.log('Song is created! ' + this.song)
        alert('Song is created!')
      })
    } else {
      alert('Title and artist are required!')
    }
  }
}
