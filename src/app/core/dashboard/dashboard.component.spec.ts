import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { DashboardComponent } from './dashboard.component'
import { RouterTestingModule } from '@angular/router/testing'
import { Router } from '@angular/router'

describe('DashboardComponent', () => {
  let component: DashboardComponent
  let fixture: ComponentFixture<DashboardComponent>
  let router: Router

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [RouterTestingModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent)
    component = fixture.componentInstance
    router = TestBed.get(Router)
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })

  it('should navigate after btn1 is clicked', () => {
    const button = fixture.debugElement.nativeElement.querySelector('.btn1')
    button.click()
    expect(router.navigate(['/dashboard/artists']))
  })

  it('should navigate after btn2 is clicked', () => {
    const button = fixture.debugElement.nativeElement.querySelector('.btn2')
    button.click()
    expect(router.navigate(['/dashboard/songs']))
  })

  it('should navigate after btn3 is clicked', () => {
    const button = fixture.debugElement.nativeElement.querySelector('.btn3')
    button.click()
    expect(router.navigate(['/dashboard/concerts']))
  })
})
