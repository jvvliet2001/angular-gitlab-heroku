import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Artist } from '../../../models/artist'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  })
}

@Injectable({
  providedIn: 'root'
})
export class ArtistService {
  constructor(private http: HttpClient) {}

  getArtists() {
    return this.http.get('http://localhost:3000/api/artists')
  }

  addArtist(artist: Artist) {
    console.log(localStorage.getItem('token'))
    if (localStorage.getItem('token')) {
      return this.http.post(
        'http://localhost:3000/api/artist',
        {
          fullName: artist.fullName
        },
        httpOptions
      )
    }
  }
}
