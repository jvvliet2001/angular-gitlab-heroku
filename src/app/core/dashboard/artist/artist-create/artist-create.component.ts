import { Component, OnInit } from '@angular/core'
import { Artist } from '../../../../models/artist'
import { ArtistService } from '../artist.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-artist-create',
  templateUrl: './artist-create.component.html',
  styleUrls: ['./artist-create.component.scss']
})
export class ArtistCreateComponent implements OnInit {
  public artist: Artist

  constructor(private artistService: ArtistService, private router: Router) {
    this.artist = new Artist()
  }

  ngOnInit() {}

  addArtist() {
    if (this.artist.fullName) {
      this.artistService.addArtist(this.artist).subscribe(res => {
        this.router.navigate(['/dashboard/artists/artist-readAll'])
        console.log('Artist is created! ' + this.artist)
        alert('Artist is created!')
      })
    } else {
      alert('Fullname is required!')
    }
  }
}
