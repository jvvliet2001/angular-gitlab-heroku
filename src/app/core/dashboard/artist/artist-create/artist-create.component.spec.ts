import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ArtistCreateComponent } from './artist-create.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { FormsModule } from '@angular/forms'
import { Router } from '@angular/router'

describe('ArtistCreateComponent', () => {
  let component: ArtistCreateComponent
  let fixture: ComponentFixture<ArtistCreateComponent>
  let router: Router

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArtistCreateComponent],
      imports: [HttpClientTestingModule, RouterTestingModule, FormsModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistCreateComponent)
    component = fixture.componentInstance
    router = TestBed.get(Router)
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })

  it('should navigate after button is clicked', () => {
    const button = fixture.debugElement.nativeElement.querySelector('.button-submit')
    button.click()
    expect(router.navigate(['/dashboard/artists/artist-readAll']))
  })
})
