import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ArtistComponent } from './artist.component'
import { RouterTestingModule } from '@angular/router/testing'

describe('ArtistComponent', () => {
  let component: ArtistComponent
  let fixture: ComponentFixture<ArtistComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArtistComponent],
      imports: [RouterTestingModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })
})
