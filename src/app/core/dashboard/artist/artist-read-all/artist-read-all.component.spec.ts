import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ArtistReadAllComponent } from './artist-read-all.component'
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'

describe('ArtistReadAllComponent', () => {
  let component: ArtistReadAllComponent
  let fixture: ComponentFixture<ArtistReadAllComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArtistReadAllComponent],
      imports: [HttpClientTestingModule, RouterTestingModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistReadAllComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })
})
