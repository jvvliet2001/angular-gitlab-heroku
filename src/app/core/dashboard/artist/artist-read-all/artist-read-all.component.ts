import { Component, OnInit } from '@angular/core'
import { ArtistService } from '../artist.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-artist-read-all',
  templateUrl: './artist-read-all.component.html',
  styleUrls: ['./artist-read-all.component.scss'],
  providers: [ArtistService]
})
export class ArtistReadAllComponent implements OnInit {
  public list: object

  constructor(private artistService: ArtistService, private router: Router) {}

  ngOnInit() {
    this.getAllArtists()
  }

  getAllArtists(): void {
    this.artistService.getArtists().subscribe(result => (this.list = result))
  }
}
