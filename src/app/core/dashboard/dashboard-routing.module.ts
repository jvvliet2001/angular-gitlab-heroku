import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './dashboard.component'
import { ArtistComponent } from './artist/artist.component'
import { SongComponent } from './song/song.component'
import { ConcertComponent } from './concert/concert.component'
import { ArtistCreateComponent } from './artist/artist-create/artist-create.component'
import { ArtistReadAllComponent } from './artist/artist-read-all/artist-read-all.component'
import { ArtistUpdateComponent } from './artist/artist-update/artist-update.component'
import { ArtistDeleteComponent } from './artist/artist-delete/artist-delete.component'
import { SongCreateComponent } from './song/song-create/song-create.component'
import { SongReadAllComponent } from './song/song-read-all/song-read-all.component'
import { SongUpdateComponent } from './song/song-update/song-update.component'
import { SongDeleteComponent } from './song/song-delete/song-delete.component'
import { ConcertCreateComponent } from './concert/concert-create/concert-create.component'
import { ConcertReadAllComponent } from './concert/concert-read-all/concert-read-all.component'
import { ConcertUpdateComponent } from './concert/concert-update/concert-update.component'
import { ConcertDeleteComponent } from './concert/concert-delete/concert-delete.component'

const dashBoardRoutes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      {
        path: 'artists',
        component: ArtistComponent,
        children: [
          { path: 'artist-create', component: ArtistCreateComponent },
          { path: 'artist-readAll', component: ArtistReadAllComponent },
          { path: 'artist-update', component: ArtistUpdateComponent },
          { path: 'artist-delete', component: ArtistDeleteComponent }
        ]
      },
      {
        path: 'songs',
        component: SongComponent,
        children: [
          { path: 'song-create', component: SongCreateComponent },
          { path: 'song-readAll', component: SongReadAllComponent },
          { path: 'song-update', component: SongUpdateComponent },
          { path: 'song-delete', component: SongDeleteComponent }
        ]
      },
      {
        path: 'concerts',
        component: ConcertComponent,
        children: [
          { path: 'concert-create', component: ConcertCreateComponent },
          { path: 'concert-readAll', component: ConcertReadAllComponent },
          { path: 'concert-update', component: ConcertUpdateComponent },
          { path: 'concert-delete', component: ConcertDeleteComponent }
        ]
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(dashBoardRoutes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
