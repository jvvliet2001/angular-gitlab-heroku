import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ConcertDeleteComponent } from './concert-delete.component'

describe('ConcertDeleteComponent', () => {
  let component: ConcertDeleteComponent
  let fixture: ComponentFixture<ConcertDeleteComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConcertDeleteComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcertDeleteComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })
})
