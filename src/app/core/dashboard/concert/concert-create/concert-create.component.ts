import { Component, OnInit } from '@angular/core'
import { Concert } from '../../../../models/concert'
import { ConcertService } from '../concert.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-concert-create',
  templateUrl: './concert-create.component.html',
  styleUrls: ['./concert-create.component.scss']
})
export class ConcertCreateComponent implements OnInit {
  public concert: Concert

  constructor(private concertService: ConcertService, private router: Router) {
    this.concert = new Concert()
  }

  ngOnInit() {}

  addConcert() {
    if (this.concert.concertName && this.concert.place && this.concert.date) {
      this.concertService.addConcert(this.concert).subscribe(res => {
        this.router.navigate(['/dashboard/concerts/concert-readAll'])
        console.log('Concert is created! ' + this.concert)
        alert('Concert is created!')
      })
    } else {
      alert('concertName, place and data are required!')
    }
  }
}
