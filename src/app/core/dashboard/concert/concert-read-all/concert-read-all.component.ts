import { Component, OnInit } from '@angular/core'
import { ConcertService } from '../concert.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-concert-read-all',
  templateUrl: './concert-read-all.component.html',
  styleUrls: ['./concert-read-all.component.scss'],
  providers: [ConcertService]
})
export class ConcertReadAllComponent implements OnInit {
  public list: object

  constructor(private concertService: ConcertService, private router: Router) {}

  ngOnInit() {
    this.getAllConcerts()
  }

  getAllConcerts(): void {
    this.concertService.getConcerts().subscribe(result => (this.list = result))
  }
}
