import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ConcertReadAllComponent } from './concert-read-all.component'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('ConcertReadAllComponent', () => {
  let component: ConcertReadAllComponent
  let fixture: ComponentFixture<ConcertReadAllComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConcertReadAllComponent],
      imports: [RouterTestingModule, HttpClientTestingModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcertReadAllComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })
})
