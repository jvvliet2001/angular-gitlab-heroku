import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ConcertComponent } from './concert.component'
import { RouterTestingModule } from '@angular/router/testing'

describe('ConcertComponent', () => {
  let component: ConcertComponent
  let fixture: ComponentFixture<ConcertComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConcertComponent],
      imports: [RouterTestingModule]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcertComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })
})
