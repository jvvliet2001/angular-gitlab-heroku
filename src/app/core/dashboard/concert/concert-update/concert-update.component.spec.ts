import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { ConcertUpdateComponent } from './concert-update.component'

describe('ConcertUpdateComponent', () => {
  let component: ConcertUpdateComponent
  let fixture: ComponentFixture<ConcertUpdateComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConcertUpdateComponent]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcertUpdateComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should instantiate', () => {
    expect(component).toBeDefined()
  })
})
