import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Concert } from '../../../models/concert'

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token')
  })
}

@Injectable({
  providedIn: 'root'
})
export class ConcertService {
  constructor(private http: HttpClient) {}

  getConcerts() {
    return this.http.get('http://localhost:3000/api/concerts')
  }

  addConcert(concert: Concert) {
    console.log(localStorage.getItem('token'))
    if (localStorage.getItem('token')) {
      return this.http.post(
        'http://localhost:3000/api/concert',
        {
          concertName: concert.concertName,
          place: concert.place,
          date: concert.date
        },
        httpOptions
      )
    }
  }
}
