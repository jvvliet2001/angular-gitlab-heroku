export class User {
  firstName: string
  lastName: string
  emailAddress: string
  mobileNumber: string
  password: string
}
