import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LoginComponent } from './authentication/login/login.component'
import { RegisterComponent } from './authentication/register/register.component'
import { ArtistComponent } from './core/dashboard/artist/artist.component'
import { SongComponent } from './core/dashboard/song/song.component'
import { ConcertComponent } from './core/dashboard/concert/concert.component'

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },

  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      { path: 'artists', component: ArtistComponent },
      { path: 'songs', component: SongComponent },
      { path: 'concerts', component: ConcertComponent }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
